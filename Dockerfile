ARG BUILD_NO=v1
FROM centos:7

LABEL author="Sergey"

SHELL ["/bin/bash", "-c"]
RUN yum install -y mc vim wget curl bash-completion && yum -y update && /bin/bash -c 'echo New Shell'.
